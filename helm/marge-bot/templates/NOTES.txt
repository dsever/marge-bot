You have successfully deployed Marge-bot!

{{- if .Values.marge_bot.force }}
We are starting the process right away
{{- end }}

{{- if .Values.marge_bot.permanent }}

Now we are permanently running Marge-bot

Enjoy it!!
{{- end }}
